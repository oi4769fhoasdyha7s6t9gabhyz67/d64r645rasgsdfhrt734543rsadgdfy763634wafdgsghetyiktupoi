/*********************************************************************
** Filename: Hallway.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Hallway.hpp"
#include "Player.hpp"

Hallway::Hallway(Player *player) : Space("hallway", hallway, player)
{
	art = new File_Art("Art/hallway.ans");
}

Hallway::~Hallway()
{
	DELETE(art);
}

void Hallway::link()
{
	std::cout << "running link() in Hallway...\n";

	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[command];
	accessible_spaces[1] = space_list[comms];
	accessible_spaces[2] = space_list[sick_bay];
	accessible_spaces[3] = space_list[engineering];
	accessible_spaces[4] = space_list[air_lock];
}

void Hallway::special()
{
	erase->draw();
	art->draw();
	char action;
	std::cout << "Player is in Hallway Space\n";
	player->set_action_pallet("coseaid");
	has_items();
	action = player->get_action();
	
	switch(action)
	{
		case 'c' :	move_player(0);
					break;
		case 'o' :	move_player(1);
					break;
		case 's' :	move_player(2);
					break;
		case 'e' :	move_player(3);
					break;
		case 'a' :	move_player(4);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
		case 'd' :	this->space_inventory->run();
					break;
	}
}
