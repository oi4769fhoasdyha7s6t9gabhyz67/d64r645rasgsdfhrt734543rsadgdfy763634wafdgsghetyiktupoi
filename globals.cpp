#include "globals.hpp"

void place_cursor(COORD position)
{
	std::cout << "\033[" << position.y << ";" << position.x << "H";
}
