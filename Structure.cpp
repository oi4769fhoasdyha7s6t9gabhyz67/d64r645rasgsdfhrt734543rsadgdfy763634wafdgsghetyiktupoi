/*********************************************************************
** Filename: Structure.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Structure.hpp"
#include <cstdlib>

Structure::Structure(Player *player_in)
{
	player = player_in;
	for (int i=0; i<Space::space_list_capacity; i++)
	{
		Space::space_list[i] = NULL;
	}

	/*
	 * Build all of the Space objects and properly place them into the master list of
	 * all spaces.
	 */

	Space::space_list[command] = new Command(player);
	Space::space_list[nav_console] = new Nav_Console(player);
	Space::space_list[systems_console] = new Systems_Console(player);
	Space::space_list[comms_console] = new Comms_Console(player);

	Space::space_list[hallway] = new Hallway(player);
	Space::space_list[comms] = new Comms(player);
	Space::space_list[sick_bay] = new Sick_Bay(player);
	Space::space_list[engineering] = new Engineering(player);
	Space::space_list[air_lock] = new Air_Lock(player);

	Space::space_list[maintenance] = new Maintenance(player);
	Space::space_list[coolant_skid] = new Coolant_Skid(player);
	Space::space_list[coolant_console] = new Coolant_Console(player);
	Space::space_list[w_drive] = new W_Drive(player);

	Space::space_list[bob_space] = new Bob(player);

	/*
	 * Set the starting position of the player
	 */
	
	player->set_space(Space::space_list[command]);

	/*
	 * set the inventory the player starts with
	 */

	player->get_inventory()->set_qty(small_kit, 1);

	/*
	 * link all the Space objects together
	 */

	for (int i=0; i<Space::space_list_capacity; i++)
	{
		if (Space::space_list[i] != NULL)
		{
			std::cout << "calling link() for index " << i << "...\n";
			Space::space_list[i]->link();
		}
	}
}

Structure::~Structure()
{
	for (int i=0; i<Space::space_list_capacity; i++)
	{
		DELETE(Space::space_list[i]);
	}
}
