/*********************************************************************
** Filename: Space_Stern.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Space_Stern.hpp"
#include "Player.hpp"

Space_Stern::Space_Stern(Player *player) : Space("space_stern", space_stern, player)
{
	art = new File_Art("Art/space_stern.ans");
	can_drop = false;
}

Space_Stern::~Space_Stern()
{
	DELETE(art);
}

void Space_Stern::link()
{
	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[air_lock];
	accessible_spaces[1] = space_list[space_port];
	accessible_spaces[2] = space_list[space_starboard];
}

void Space_Stern::special()
{
	erase->draw();
	art->draw();
	char action;
	player->set_action_pallet("apsi");

	action = player->get_action();
	
	switch(action)
	{
		case 'a' :	move_player(0);
					break;
		case 'p' :	move_player(1);
					break;
		case 's' :	move_player(2);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
	}
}

