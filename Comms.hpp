/*********************************************************************
** Filename: Comms.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef COMMS_HPP
#define COMMS_HPP

#include "Space.hpp"

class Comms : public Space
{
	private:
	
		File_Art *art;
		
	public:
		
		Comms(Player *player);
		virtual ~Comms();
		virtual void special();
		virtual void link();

};
#endif
