/*********************************************************************
** Filename: Command.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Command.hpp"
#include "Player.hpp"
#include "Inventory.hpp"

Command::Command(Player *player) : Space("command", command, player)
{
	command_art = new File_Art("Art/command.ans");
}

Command::~Command()
{
	DELETE(command_art);
}

void Command::link()
{
	std::cout << "running link() in Command...\n";

	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[nav_console];
	accessible_spaces[1] = space_list[systems_console];
	accessible_spaces[2] = space_list[comms_console];
	accessible_spaces[3] = space_list[hallway];
	accessible_spaces[4] = space_list[bob_space];
}

void Command::special()
{
	erase->draw();
	command_art->draw();
	char action;
	std::string action_pallette = "nschbid";
	COORD BOB_MESSAGE = {95,55};

	/*
	 * We delete bob's space after we pick him up.  No harm in deleting NULL
	 * over and over again...
	 */

	if ((bob_in_bed == true) || (player->get_inventory()->has_item(1) == true))
	{
		action_pallette.erase(action_pallette.find('b',0), 1);
		accessible_spaces[4] = NULL;		// prevent dangling pointer...
		DELETE(space_list[bob_space]);
		place_cursor(BOB_MESSAGE);
		std::cout << "\033[34mBob's command space has be removed/deleted...\033[0m";
	}
	else
	{
		place_cursor(BOB_MESSAGE);
		std::cout << "\033[35mEnter \033[32mb\033[35m to go to bob's command space\033[0m";
	}
	player->set_action_pallet(action_pallette);
	has_items();
	action = player->get_action();
	
	switch(action)
	{
		case 'n' :	move_player(0);
					break;
		case 's' :	move_player(1);
					break;
		case 'c' :	move_player(2);
					break;
		case 'h' :	move_player(3);
					break;
		case 'b' :	move_player(4);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
		case 'd' :	this->space_inventory->run();
					break;
	}
}
