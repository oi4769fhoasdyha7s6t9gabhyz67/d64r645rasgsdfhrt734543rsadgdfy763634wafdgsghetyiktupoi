/*********************************************************************
** Filename: Maintenance.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Maintenance.hpp"
#include "Player.hpp"

Maintenance::Maintenance(Player *player) : Space("maintenance", maintenance, player)
{
	art = new File_Art("Art/maintenance.ans");

	/* This space starts out with the following items */

	this->get_inventory()->set_qty(welder, 1);
	this->get_inventory()->set_qty(good_valve, 1);
	this->get_inventory()->set_qty(toolbox, 1);
	this->get_inventory()->set_qty(scrap, 1);
}

Maintenance::~Maintenance()
{
	DELETE(art);
}

void Maintenance::link()
{
	std::cout << "running link() in Maintenance...\n";

	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[engineering];
}

void Maintenance::special()
{
	erase->draw();
	art->draw();
	char action;
	std::cout << "Player is in Maintenance Space\n";
	player->set_action_pallet("eid");
	has_items();
	action = player->get_action();
	
	switch(action)
	{
		case 'e' :	move_player(0);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
		case 'd' :	this->space_inventory->run();
					break;
	}
}
