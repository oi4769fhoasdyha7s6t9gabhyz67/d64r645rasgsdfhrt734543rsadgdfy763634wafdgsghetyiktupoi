/*********************************************************************
** Filename: Structure.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef STRUCTURE_HPP
#define STRUCTURE_HPP

#include "Player.hpp"

#include "Space.hpp"
#include "Command.hpp"
#include "Nav_Console.hpp"
#include "Comms_Console.hpp"
#include "Systems_Console.hpp"


#include "Hallway.hpp"
#include "Comms.hpp"
#include "Sick_Bay.hpp"
#include "Engineering.hpp"
#include "Air_Lock.hpp"

#include "Maintenance.hpp"
#include "Coolant_Skid.hpp"
#include "Coolant_Console.hpp"
#include "W_Drive.hpp"

#include "Bob.hpp"

#include "Space_Stern.hpp"
#include "Space_Port.hpp"
#include "Space_Starboard.hpp"


class Structure
{
	private:

		Player *player;
	public:
		Structure(Player *player);
		~Structure();
};

#endif
