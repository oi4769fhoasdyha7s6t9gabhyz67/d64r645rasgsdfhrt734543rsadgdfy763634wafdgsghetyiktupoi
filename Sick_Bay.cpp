/*********************************************************************
** Filename: Sick_Bay.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Sick_Bay.hpp"
#include "Player.hpp"

Sick_Bay::Sick_Bay(Player *player) : Space("sick_bay", sick_bay, player)
{
	art = new File_Art("Art/sick_bay.ans");
	art_bob = new File_Art("Art/sick_bay_bob.ans");
}

Sick_Bay::~Sick_Bay()
{
	DELETE(art);
	DELETE(art_bob);
}

void Sick_Bay::link()
{
	std::cout << "running link() in Sick_Bay...\n";

	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[hallway];
}

void Sick_Bay::special()
{
	if (space_inventory->has_item(bob) == true)
	{
		bob_in_bed = true;
		space_inventory->set_qty(bob, 0);
		DELETE(art);
		art = art_bob;	
		art_bob = NULL;
	}
	erase->draw();
	art->draw();
	char action;
	std::cout << "Player is in Sick_Bay Space\n";
	player->set_action_pallet("hid");
	has_items();
	action = player->get_action();
	
	switch(action)
	{
		case 'h' :	move_player(0);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
		case 'd' :	this->space_inventory->run();
					break;
	}
}
