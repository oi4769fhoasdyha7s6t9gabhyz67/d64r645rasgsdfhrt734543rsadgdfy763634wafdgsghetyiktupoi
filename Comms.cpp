/*********************************************************************
** Filename: Comms.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Comms.hpp"
#include "Player.hpp"

Comms::Comms(Player *player) : Space("comms", comms, player)
{
	art = new File_Art("Art/comms.ans");
}

Comms::~Comms()
{
	DELETE(art);
}

void Comms::link()
{
	std::cout << "running link() in Comms...\n";

	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[hallway];
}

void Comms::special()
{
	erase->draw();
	art->draw();
	char action;
	player->set_action_pallet("hid");
	has_items();
	action = player->get_action();
	accessed_comms_room = true;
	
	switch(action)
	{
		case 'h' :	move_player(0);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
		case 'd' :	this->space_inventory->run();
					break;
	}
}
