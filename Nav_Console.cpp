/*********************************************************************
** Filename: Nav_Console.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Nav_Console.hpp"
#include "Player.hpp"

Nav_Console::Nav_Console(Player *player) : Space("nav_console", nav_console, player)
{
	bad = new File_Art("Art/nav_console_bad.ans");
	good = new File_Art("Art/nav_console_good.ans");
	art = bad;
	can_drop = false;
	message = new File_Art("Art/nav_message.txt", "\033[0m", TERMINAL_MESSAGE.x,
		TERMINAL_MESSAGE.y);
}

Nav_Console::~Nav_Console()
{
	art = NULL;
	DELETE(bad);
	DELETE(good);
	DELETE(message);
}

void Nav_Console::link()
{
	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[command];
}

void Nav_Console::special()
{
	if (comms_online == true)
	{
		art = good;
	}
	erase->draw();
	art->draw();	
	message->draw();
	char action;
	player->set_action_pallet("cf");
	action = player->get_action();
	
	switch(action)
	{
		case 'c' :	move_player(0);
					break;
		case 'f' :	win_check();
					break;
	}
}

void Nav_Console::win_check()
{
	if (bob_in_bed &&
		(comms_online && coolant_online))
	{
		/* you win! */
		player->set_won(true);
		std::cout << "\033[1;32mYOU WIN, YOU ARE FLYING HOME AND SAVING BOB!!!!\033[0m";
		io2::enter();
	}
	else
	{
		std::cout << "There is still more work to do before you can fly home...";
		io2::enter();
	}
}
