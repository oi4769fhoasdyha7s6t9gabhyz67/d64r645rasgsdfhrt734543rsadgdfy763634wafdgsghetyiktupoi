/*********************************************************************
** Filename: Coolant_Skid.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Coolant_Skid.hpp"
#include "Player.hpp"

Coolant_Skid::Coolant_Skid(Player *player) : Space("coolant_skid", coolant_skid, player)
{
	art = new File_Art("Art/coolant_skid.ans");
}

Coolant_Skid::~Coolant_Skid()
{
	DELETE(art);
}

void Coolant_Skid::link()
{
	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[engineering];
}

void Coolant_Skid::special()
{
	erase->draw();
	art->draw();
	char action;
	std::string action_pallete = "eid";
	has_items();

	if (leak == false && valve_replaced == false)
	{
		action_pallete += 'r';
		COORD message = {12,57};
		place_cursor(message);
		std::cout << "\033[1;35mEnter \033[33mr\033[1;35m to replace RR-VALVE" <<
		std::endl << "\033[" << message.x << "G";
		std::cout << "*****************************\033[0m";
	}


	player->set_action_pallet(action_pallete);
	action = player->get_action();
	
	switch(action)
	{
		case 'e' :	move_player(0);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
		case 'd' :	this->space_inventory->run();
					break;
		case 'r' :	replace_valve();
					break;
	}
}

void Coolant_Skid::replace_valve()
{

	/* check if player has toolbox and good_valve */

	if 
	(
			player->get_inventory()->has_item(4)
		&&	player->get_inventory()->has_item(2)
	)
	{
		player->get_inventory()->remove_item(4);	// remove the valve
		this->get_inventory()->set_qty(good_valve,0);		// make the valve innaccessible
		valve_replaced = true;
	}
	else
	{
		std::cout << "\033[1;31mCan't Replace.  You need the toolbox and a good valve..."
			<< "\033[0m";
		io2::enter();
	}
}
