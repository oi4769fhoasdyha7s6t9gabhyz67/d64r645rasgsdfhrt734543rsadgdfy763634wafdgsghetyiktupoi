/*********************************************************************
** Filename: Inventory.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Space_Inventory.hpp"
#include "Space.hpp"

Space_Inventory::Space_Inventory(Player *player) 
: Inventory(
	player,
	"Art/space_inventory.txt",
	"\033[0m",
	INVENTORY.x,
	INVENTORY.y
)
{
	carry_capacity = 100;
}

/* removes the specified item from the inventory */

void Space_Inventory::remove_item(int tag)
{
	if (player->get_inventory()->add_item(tag) == true)
	{
		items[tag]->qty = 0;
	}
}

