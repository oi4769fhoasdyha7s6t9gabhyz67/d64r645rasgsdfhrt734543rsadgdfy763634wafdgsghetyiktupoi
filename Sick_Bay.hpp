/*********************************************************************
** Filename: Sick_Bay.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef SICK_BAY_HPP
#define SICK_BAY_HPP

#include "Space.hpp"

class Sick_Bay : public Space
{
	private:
	
		File_Art *art;
		File_Art *art_bob;
		
	public:
		
		Sick_Bay(Player *player);
		virtual ~Sick_Bay();
		virtual void special();
		virtual void link();

};
#endif
