/*********************************************************************
** Filename: Comms_Console.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef COMMS_CONSOLE_HPP
#define COMMS_CONSOLE_HPP

#include "Space.hpp"

class Comms_Console : public Space
{
	private:

		File_Art *bad;
		File_Art *good;
		File_Art *art;
		
	public:
		
		Comms_Console(Player *player);
		virtual ~Comms_Console();
		virtual void special();
		void link();
		void uplink();
};
#endif
