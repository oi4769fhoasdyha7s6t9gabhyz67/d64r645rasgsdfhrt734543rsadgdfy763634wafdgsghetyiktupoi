/*********************************************************************
** Filename: 
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Player.hpp"
#include "Space.hpp"
#include "io2.hpp"
#include <iostream>
#include "globals.hpp"
#include "Inventory.hpp"

Player::Player()
{
	space = NULL;
	action_pallete = "";
	inventory = new Inventory(this);
	won = false;
	timer = 0;
	limit = 200;
}

Player::~Player()
{
	DELETE(inventory);
}

char Player::get_action()
{
	bool valid_action;
	std::string player_input;
	do
	{
		place_cursor(ACTION_PRMT_POS);
		std::cout << "\033[0J";
		std::cout << limit - timer << " turn left.. ";
		std::cout << "\033[33mPlease [Enter] you next action: \033[0m";

		if (action_pallete.find('i') != std::string::npos)
		{
			std::cout << "(enter 'i' for inventory) ";
		}

		std::getline(std::cin, player_input);
		if (input_valid(player_input) == false)
		{
			std::cout << "\033[31mOnly lower case letters and numbers are valid. " 
			<< "\033[0m";
			io2::enter();
			valid_action = false;
		}
		else if (action_pallete.find(player_input[0]) == std::string::npos)
		{
			std::cout << "\033[31mNot a valid action [only '" << 
			"\033[1;35m" << action_pallete << "\033[31m" << std::flush <<
			"' available].  Please choose again. \033[0m";
			io2::enter();
			valid_action = false;
		}
		else
		{
			valid_action = true;
		}
		place_cursor(ACTION_MESSAGE_POS);
		std::cout << "\033[0J";
	}
	while (valid_action == false);
	return player_input[0];
}

bool Player::input_valid(std::string s)
{
	if ((s == "") || (s.length() > 1))
	{
		return false;
	}
	else if ((s[0] >= '0' && s[0] <= '9') || (s[0] >='a' && s[0] <= 'z'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Player::set_action_pallet(std::string action_pallete)
{
	this->action_pallete = action_pallete;
}

void Player::set_space(Space *space)
{
	timer++;
	this->space = space;
}

Space * Player::get_space()
{
	return space;
}

std::string Player::get_action_pallette()
{
	return action_pallete;
}

Inventory * Player::get_inventory()
{
	return inventory;
}

bool Player::get_won()
{
	return won;
}

void Player::set_won(bool won)
{
	this->won = won;
}

int Player::get_timer()
{
	return timer;
}

int Player::get_limit()
{
	return limit;
}
