/*********************************************************************
** Filename: Systems_Console.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Systems_Console.hpp"
#include "Player.hpp"

Systems_Console::Systems_Console(Player *player) : Space("systems_console", systems_console, player)
{
	bad = new File_Art("Art/systems_console.ans");
	can_drop = false;
}

Systems_Console::~Systems_Console()
{
	DELETE(bad);
}

void Systems_Console::link()
{
	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[command];
}

void Systems_Console::special()
{
	erase->draw();
	bad->draw();
		
	if (comms_online == false)
	{
		place_cursor(TERMINAL_MESSAGE);
		std::cout << ">>> Comms are offline";
	}

	if (coolant_online == false)
	{
		place_cursor(TERMINAL_MESSAGE);
		std::cout << std::endl << "\033[" << TERMINAL_MESSAGE.x << "G";
		std::cout << ">>> Coolant is Offline";
	}
	
	place_cursor(TERMINAL_MESSAGE);
	std::cout <<  "\033[4B";
	std::cout << ">>> Enter 'c' to return to command space...";

	char action;
	player->set_action_pallet("c");
	action = player->get_action();


	
	switch(action)
	{
		case 'c' :	move_player(0);
					break;
	}
}


