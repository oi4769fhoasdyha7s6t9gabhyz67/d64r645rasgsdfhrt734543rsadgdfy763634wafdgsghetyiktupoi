/*********************************************************************
** Filename: Command.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef COMMAND_HPP
#define COMMAND_HPP

#include "Space.hpp"

class Command : public Space
{
	private:
		
		File_Art *command_art;
	
	public:
		
		Command(Player *player);
		virtual ~Command();
		virtual void special();
		virtual void link();
		void bob();
};
#endif
