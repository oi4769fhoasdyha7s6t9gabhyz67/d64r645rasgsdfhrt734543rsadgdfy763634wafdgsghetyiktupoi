/*********************************************************************
** Filename: Hallway.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef HALLWAY_HPP
#define HALLWAY_HPP

#include "Space.hpp"

class Hallway : public Space
{
	private:

		File_Art *art;

	public:
		
		Hallway(Player *player);
		virtual ~Hallway();
		virtual void special();
		virtual void link();

};
#endif
