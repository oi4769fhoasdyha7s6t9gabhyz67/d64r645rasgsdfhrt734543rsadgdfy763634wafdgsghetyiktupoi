/*********************************************************************
** File Name: io2.hpp
** Author: Kruno Peric
** Date: 7/3/2016
** Description: Interface to <iostream> wrapper function library.
** Parameters: 
** Pre-Conditions:
** Post-Conditions:
*********************************************************************/

#ifndef IO2_HPP
#define IO2_HPP

#include <string>
#include <climits>

namespace io2
{
	/*
	 * Takes a string as a parameter and returns true if the string is an 
	 * integer.  Returns false otherwise.
	 */

	bool is_int(std::string string_in, bool error_message = false);

	/*
	 * Converts a string to a double and returns the double.  Pre-Condition that 
	 * the string may ONLY contain numbers.
	 */

	double string_to_double(std::string string_in);

	/*
	 * Takes a double and boundries as parameters.  Returns true if the double
	 * is within the integer boundaray limits [MIN..MAX].  Returns false otherwise.
	 */

	bool dbl_valid_range(double value, const int MIN  = INT_MIN, const int MAX =
	INT_MAX, bool error_message = true);

	/*
	 * Extracts input stream and returns an integer. Validation is enforced for 
	 * io2::is_int() and io2::dbl_valid_range().
	 */

	int int_range_in(int a = INT_MIN, int b = INT_MAX);

	/*
	 * Exracts input stream and returns a string.  The extraction will terminate
	 * at space and endline characters.
	 */

	std::string cin_string_no_space(std::string prompt = "");

	/*
	 * Extracts a char from input stream and returns it.  Function has optoins for
	 * handling user specific prompt or no prompt.
	 */

	char cin_ch(bool stand_message = true, std::string prompt = "");
	
	/*
	 * enter() just continues after the user enters anything...
	 */
	
	void enter();
		
}

#endif
