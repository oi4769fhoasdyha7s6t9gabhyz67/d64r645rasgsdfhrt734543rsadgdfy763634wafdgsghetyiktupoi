/*********************************************************************
** Filename: Inventory.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "globals.hpp"
#include "io2.hpp"
#include "Player.hpp"
#include <iomanip>
#include "Space.hpp"
#include "Inventory.hpp"
#include "Space_Inventory.hpp"

Inventory::Inventory
(
	Player *player,
	std::string file ,
	std::string color, 
	int x,
	int y 
)
: inventory_screen(file, color, x, y)
{
	num_items = 8;
	items = new Item * [num_items];

	items[0] = NULL;
	items[bob] = new Item("Bob", new File_Art("Art/bob.ans"), 3);
	items[toolbox] = new Item("Toolbox", new File_Art("Art/toolbox.ans"), 2);
	items[welder] = new Item("Space Welder", new File_Art("Art/welder.ans"), 2);
	items[good_valve] = new Item("Valve", new File_Art("Art/good_valve.ans"), 1);
	items[brocken_valve] = new Item("Brocken Valve", new File_Art("Art/brocken_valve.ans"),
		1);
	items[scrap] = new Item("Scrap Metal", new File_Art("Art/scrap.ans"), 1);
	items[small_kit] = new Item("Small Tool Kit", new File_Art("Art/small_kit.ans"), 1);

	carry_capacity = 3;
	
	this->player = player;
}



Inventory::~Inventory()
{
	for (int i=0; i<num_items; i++)
	{
		DELETE(items[i]);
	}

	delete [] items;
	items = NULL;
}


/* the total weight of the inventory */

int Inventory::get_weight()
{
	int acc = 0;
	for (int i=1; i<num_items; i++)
	{
		acc+=items[i]->qty * items[i]->slot_footprint;
	}
	return acc;
}

/* adds an item if the the weight does not go over */

bool Inventory::add_item(int tag)
{
	if ((get_weight() + items[tag]->slot_footprint) > carry_capacity)
	{
		place_cursor(ACTION_PRMT_POS);
		std::cout << "\033[31mItem not added.  You are Carrying too much...\033[0m";
		io2::enter();
		return false;
	}
	else
	{
		items[tag]->qty = 1;
		return true;
	}
}

/* removes the specified item from the inventory */

void Inventory::remove_item(int tag)
{
	if (player->get_space()->get_can_drop() == true)
	{
		items[tag]->qty = 0;
		player->get_space()->get_inventory()->add_item(tag);
	}
	else
	{
		std::cout << "\033[35mYou can't drop items in this area, try" << 
			" another area in the ship\033[0m";
	}
}

/* prints the inventory */

void Inventory::show_inventory()
{
	inventory_screen.draw();

	/* display inventory lister header... */

	place_cursor(INVENTORY_LIST);
	std::cout << "\033[1;33m";
	std::cout << std::setw(10) << "ITEM NO." ;
	std::cout << std::setw(20) << "ITEM        " ;
	std::cout << std::setw(15) << "FOOTPRINT";
	std::cout << "\033[0m" << std::endl;

	/* print each item in the inventory */

	for (int i=1; i<num_items; i++)
	{
		if (items[i]->qty == 1)
		{
			std::cout << "\033[" << INVENTORY_LIST.x << "G" << std::flush;
			std::cout <<  "\033[1;32m" << std::setw(10) <<  i << "\033[0m";
			std::cout << std::setw(20) << items[i]->name;
			std::cout << std::setw(15) << items[i]->slot_footprint << std::endl;
		}
	}
	std::cout << std::endl;
	std::cout << "\033[" << INVENTORY_LIST.x << "G" << std::flush;
	std::cout << "            Total Footprint = " << get_weight();
	std::cout << ",  Capcity = " << carry_capacity << std::endl;
}

/* Runs then inventory program */

void Inventory::run()
{
	do
	{
		show_inventory();
		std::string temp = player->get_action_pallette();	// Store Player's current action pallette
		std::string action_pallette = get_items_tag_string();
		player->set_action_pallet(action_pallette);	// set the action pallette to inventory
		char action = player->get_action();

		if (action == bob + '0')
		{
			if (player->get_space()->get_space_id() != sick_bay)
			{
				std::cout << "\033[35mYou must place bob in the sick bay, not here!\033[0m";
			}
			else
			{
				remove_item(action - '0');
			}
		}
		else if (action == '0')
		{
			return;
		}
		else
		{
			remove_item(action - '0');
		}
	}
	while (true);
}

/* returns a string containing the enum tag or each item in the inventory and '0', for
 * exiting */

std::string Inventory::get_items_tag_string()
{
	std::string s = "";
	for (int i=1; i<num_items; i++)
	{
		if (items[i]->qty == 1)
		{
			s += i + '0';
		}
	}
	return s + '0';
}

/* checks if the item is in the inventory */

bool Inventory::has_item(int tag) 
{
	if (items[tag]->qty == 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}


void Inventory::set_qty(Item_tag tag, int qty)
{
	items[tag]->qty = qty;
}
