/*********************************************************************
** Filename: 
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Space.hpp"
#include "Player.hpp"
#include <cstdlib>

const int space_list_capacity = 100;
Space* Space::space_list[space_list_capacity];
bool Space::bob_in_bed = false;
bool Space::panel_fixed = false;
bool Space::leak = true;
bool Space::valve_replaced = false;
bool Space::coolant_online = false;
bool Space::bad_valve = true;
bool Space::comm_sat_fixed = false;
bool Space::comms_online = false;
bool Space::accessed_coolant_console = false;
bool Space::accessed_comms_room = false;
bool Space::accessed_comms_console = false;

Space::Space(std::string tag_name, space_id ID, Player *player)
{
	this->tag_name = tag_name;
	this->ID = ID;
	space_list[ID] = this;

	for (int i=0; i<10; i++)
	{
		accessible_spaces[i] = NULL;
	}

	can_drop = true;
	this->player = player;
	space_inventory = new Space_Inventory(player);
	erase = new File_Art("Art/erase.ans");
}

Space::~Space()
{
	DELETE(space_inventory);
	DELETE(erase);
}

void Space::move_player(int accessible_spaces_index)
{
	/* Move the player to the adjacent space... */

	player->set_space(accessible_spaces[accessible_spaces_index]);
}

space_id Space::get_space_id()
{
	return ID;
}

Inventory * Space::get_inventory()
{
	return space_inventory;
}

bool Space::get_can_drop()
{
	return can_drop;
}

void Space::has_items()
{
	if (space_inventory->get_weight() > 0)
	{
		place_cursor(ACTION_MESSAGE_POS);
		std::cout << "\033[2K";
		std::cout << "\033[32mThere are items in the space, [enter] 'd' to look at"
			<<" them.\033[0m";
	}
}

bool Space::player_has(Item_tag tag)
{
	return get_player()->get_inventory()->has_item(tag);
}

Player * Space::get_player()
{
	return player;
}
