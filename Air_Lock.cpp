/*********************************************************************
** Filename: Air_Lock.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Air_Lock.hpp"
#include "Player.hpp"
#include "Space_Stern.hpp"
#include "Space_Port.hpp"
#include "Space_Starboard.hpp"

COORD Air_Lock::AIR_LOCK_MESSAGE = {65,45};

Air_Lock::Air_Lock(Player *player) : Space("air_lock", air_lock, player)
{
	art = new File_Art("Art/air_lock.ans");
	added_outside = false;	// adding outside nodes will only happen once...
	deleted_outside = false;
}

Air_Lock::~Air_Lock()
{
	DELETE(art);
}

void Air_Lock::link()
{
	std::cout << "running link() in Air_Lock...\n";

	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[hallway];
}

void Air_Lock::special()
{
	erase->draw();
	art->draw();
	check_objectives();
	add_outside_spaces();
	char action;
	player->set_action_pallet("hids");
	has_items();
	action = player->get_action();
	
	switch(action)
	{
		case 'h' :	move_player(0);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
		case 'd' :	this->space_inventory->run();
					break;
		case 's' :	space_walk_validity();
					break;
	}
}

void Air_Lock::add_outside_spaces()
{
	if (added_outside == false)
	{
		if (
				(accessed_coolant_console && accessed_comms_console) &&
				(accessed_comms_room)
		)	
		{
			place_cursor(AIR_LOCK_MESSAGE);
			std::cout << "\033[1;32mAdding new outside Spaces to the linked structure...";
			io2::enter();

			/* Create the new objects that are outside of the ship... */

			space_list[space_stern] = new Space_Stern(player);
			space_list[space_port] = new Space_Port(player);
			space_list[space_starboard] = new Space_Starboard(player);

			/* Link the new objects... */

			space_list[space_stern]->link();
			space_list[space_port]->link();
			space_list[space_starboard]->link();
			accessible_spaces[1] = space_list[space_stern];

			/* flag the process as having previously occurred... */
			
			added_outside = true;	// now it will never happen again...
		}
	}
}

void Air_Lock::space_walk_validity()
{
	if (added_outside == false)
	{
		place_cursor(AIR_LOCK_MESSAGE);
		std::cout << "\033[0K" << std::flush;
		std::cout << "\033[1;32mI should probably find out more before I go on " <<
			"a space walk..." << std::endl << std::endl;;
		std::cout << "\033[" << AIR_LOCK_MESSAGE.x << "G";
		std::cout << "\033[1;32mHINT:  Investigate the Coolant Console, Comms Console,\n";
		std::cout << "\033[" << AIR_LOCK_MESSAGE.x << "G";
		std::cout << "and Comms room first.  Then you can go outside....\033[0m" <<
		std::endl <<  "\033[" << AIR_LOCK_MESSAGE.x << "G";
		io2::enter();
	}
	else if ((panel_fixed == false) || (comm_sat_fixed == false))
	{
		/* 
		 * If the Space objects have been added and if it is still necessary to go
		 * outside for repairs, we will move the player..
		 */

		move_player(1);
	}
	else
	{
		place_cursor(AIR_LOCK_MESSAGE);
		std::cout << "\033[0K" << std::flush;
		std::cout << "\033[1;32mThere is no more reason to go outside..." <<
			std::endl << std::endl;
		std::cout << "\033[" << AIR_LOCK_MESSAGE.x << "G";
		std::cout << "\033[1;32mThe objects Space_Stern, Space_Port, \n";
		std::cout << "\033[" << AIR_LOCK_MESSAGE.x << "G";
		std::cout << "and and Space_Starboard have been deleted...\033[0m" <<
		std::endl <<  "\033[" << AIR_LOCK_MESSAGE.x << "G";
		io2::enter();
	}
}

void Air_Lock::check_objectives()
{
	if (panel_fixed && comm_sat_fixed)
	{
		if (deleted_outside == false)
		{
			place_cursor(AIR_LOCK_MESSAGE);
			std::cout << "\033[0K" << std::flush;
			std::cout << "\033[1;32mThere is no more reason to go outside..." <<
				std::endl << std::endl;
			std::cout << "\033[" << AIR_LOCK_MESSAGE.x << "G";
			std::cout << "\033[1;32mDeleting objects Space_Stern, Space_Port, Space_Starboard\n";
			io2::enter();

			/* Remove the outside structure */

			accessible_spaces[1] = NULL;		// prevent dangling pointer...
			DELETE(space_list[space_stern]);
			DELETE(space_list[space_port]);
			DELETE(space_list[space_starboard]);
			deleted_outside = true;				// no need to keep processing...
		}
	}
}
