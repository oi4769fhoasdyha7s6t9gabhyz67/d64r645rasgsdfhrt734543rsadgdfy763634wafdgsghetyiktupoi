/*********************************************************************
** Filename: Inventory.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef INVENTORY_HPP
#define INVENTORY_HPP

#include <string>
#include <fstream>
#include "File_Art.hpp" 
#include "globals.hpp" 

enum Item_tag { bob = 1, toolbox = 2, welder = 3, good_valve = 4, brocken_valve = 5, 
				scrap = 6, small_kit = 7 };

struct Item
{
	std::string name;
	File_Art *art;
	int slot_footprint;
	int qty;
	Item (std::string name, File_Art *art, int slot_footprint)
	{
		this->name = name;
		this->art = art;
		this->slot_footprint = slot_footprint;
		qty = 0;
	}
	~Item()
	{
		DELETE(art);
	}
};

class Player;		// forward declaration Since both classes has each other

class Inventory
{
	protected:
		int num_items;
		Item **items;	
		int carry_capacity;
		File_Art inventory_screen;
		std::string always_removable;
		Player *player;
	public:
		int get_weight();
		bool add_item(int);
		virtual void remove_item(int);
		void show_inventory();
		void run();
		bool has_item(int tag);
		void set_qty(Item_tag tag, int qty);
		std::string get_items_tag_string();
		Inventory
		(
			Player *player,
			std::string file =  "Art/inventory_screen.txt",
			std::string color =  "\033[35m", 
			int x = INVENTORY.x,
			int y = INVENTORY.y
		);
		
		virtual ~Inventory();
};

#endif
