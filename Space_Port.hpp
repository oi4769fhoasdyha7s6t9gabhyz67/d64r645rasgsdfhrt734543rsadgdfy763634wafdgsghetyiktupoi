/*********************************************************************
** Filename: Space_Port.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef SPACE_PORT_HPP
#define SPACE_PORT_HPP

#include "Space.hpp"

class Space_Port : public Space
{
	private:
	
		File_Art *bad;
		File_Art *good;
		File_Art *art;
	
	public:
		
		Space_Port(Player *player);
		virtual ~Space_Port();
		virtual void special();
		void link();
		void weld_panel();
};
#endif
