/*********************************************************************
** Filename: Coolant_Skid.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef COOLANT_SKID_HPP
#define COOLANT_SKID_HPP

#include "Space.hpp"

class Coolant_Skid : public Space
{
	private:

		File_Art *art;
		
	public:
		
		Coolant_Skid(Player *player);
		virtual ~Coolant_Skid();
		virtual void special();
		virtual void link();
		void replace_valve();

};
#endif
