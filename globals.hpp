#ifndef GLOBALS_HPP
#define GLOBALS_HPP

#include <iostream>

#define DELETE(x) { delete x; x = NULL; }

struct COORD
{
	short int x;	// Column position
	short int y;	// Row position
};

void place_cursor(COORD position);

const COORD ACTION_PRMT_POS = { 1, 63 };
const COORD ACTION_MESSAGE_POS = { 1, 62 };
const COORD DEFAULT_POS = { 1, 1 };
const COORD TERMINAL_MESSAGE = { 80, 51 };
const COORD TERMINAL_MAIN = { 80, 6 };
const COORD INVENTORY  = { 60, 15 };
const COORD INVENTORY_LIST  = { 65 , 25 };

#endif
