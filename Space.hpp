/*********************************************************************
** Filename: Space.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef SPACE_HPP
#define SPACE_HPP

#include <iostream>
#include "io2.hpp"
#include "Space_Inventory.hpp"



enum space_id 	
{
	command = 0,	nav_console = 1, systems_console = 2, comms_console = 3,
	hallway = 4,	comms = 5, sick_bay = 6, engineering = 7, air_lock = 8,
	maintenance = 9, coolant_skid = 10, coolant_console = 11, w_drive = 12,
	bob_space = 13, space_stern = 14, space_port = 15, space_starboard = 16
};

class Player;	// Forward declaration since they have each other

class Space
{
	protected:

		friend class Structure;

		/* 
		 * space_list is a master list of pointers to evey instantiated Space
		 * object.
		 */

		static const int space_list_capacity = 100;
		static Space *space_list[space_list_capacity];
		static bool bob_in_bed;
		static bool panel_fixed;
		static bool leak;
		static bool comm_sat_fixed;
		static bool valve_replaced;
		static bool coolant_online;
		static bool bad_valve;
		static bool comms_online;
		static bool accessed_coolant_console;
		static bool accessed_comms_room;
		static bool accessed_comms_console;
		
		/* name tags ascociated with space. */

		std::string tag_name;	

		/* pointers to other game spaces */
		
		Space *accessible_spaces[10];

		space_id ID;

		Player *player;
	
		void virtual move_player(int accessible_spaces_index);

		Inventory *space_inventory;

		bool can_drop;
		
		File_Art *erase;	

		
	public:
		
		Space(std::string tag_name, space_id, Player *);

		space_id get_space_id();

		Inventory * get_inventory();
		
		bool get_can_drop();

		void has_items();
		
		bool player_has(Item_tag tag);

		Player * get_player();

		/* special si the unique behavior for each type of space */

		virtual void special() = 0;

		virtual void link() = 0;

		virtual ~Space();
};
#endif
