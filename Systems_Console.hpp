/*********************************************************************
** Filename: Systems_Console.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef SYSTEMS_CONSOLE_HPP
#define SYSTEMS_CONSOLE_HPP

#include "Space.hpp"

class Systems_Console : public Space
{
	private:
	
		File_Art *bad;
		
	public:
		
		Systems_Console(Player *player);
		virtual ~Systems_Console();
		virtual void special();
		void link();
		
};
#endif
