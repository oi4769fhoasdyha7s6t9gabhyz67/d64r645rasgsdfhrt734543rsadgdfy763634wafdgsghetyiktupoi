/*********************************************************************
** Filename: W_Drive.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef W_DRIVE_HPP
#define W_DRIVE_HPP

#include "Space.hpp"

class W_Drive : public Space
{
	private:
		File_Art *art;
	public:
		W_Drive(Player *player);
		virtual ~W_Drive();
		virtual void special();
		virtual void link();
};
#endif
