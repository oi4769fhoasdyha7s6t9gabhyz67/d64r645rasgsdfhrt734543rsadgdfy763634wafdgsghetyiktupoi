/*********************************************************************
** Filename: Space_Port.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Space_Port.hpp"
#include "Player.hpp"

Space_Port::Space_Port(Player *player) : Space("space_port", space_port, player)
{
	bad = new File_Art("Art/space_port.ans");
	good = new File_Art("Art/space_port_fixed.ans");
	art = bad;
	can_drop = false;
}

Space_Port::~Space_Port()
{
	art = NULL;
	DELETE(bad);
	DELETE(good);
}

void Space_Port::link()
{
	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[space_stern];
}

void Space_Port::special()
{
	erase->draw();
	art->draw();
	char action;
	std::string action_pallete = "si";
	if (panel_fixed == false)
	{
		action_pallete += 'w';
	}
	player->set_action_pallet(action_pallete);

	action = player->get_action();
	
	switch(action)
	{
		case 's' :	move_player(0);
					break;
		case 'w' :	weld_panel();
					break;
		case 'i' :	player->get_inventory()->run();
					break;
	}
}

void Space_Port::weld_panel()
{
	if (player->get_inventory()->has_item(3) == true
		&& player->get_inventory()->has_item(6) == true)
	{
		panel_fixed = true;
		art = good;
	}
	else
	{
		std::cout << "\033[1;31mYou need both scrap metal and a welder to fix this..." <<
			"\033[0m";
		io2::enter();
	}
}
