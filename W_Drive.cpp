/*********************************************************************
** Filename: W_Drive.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "W_Drive.hpp"
#include "Player.hpp"

W_Drive::W_Drive(Player *player) : Space("w_drive", w_drive, player)
{
	art = new File_Art("Art/w_drive.ans");
}

W_Drive::~W_Drive()
{
	DELETE(art);
}

void W_Drive::link()
{
	std::cout << "running link() in W_Drive...\n";

	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[engineering];
}

void W_Drive::special()
{
	erase->draw();
	art->draw();
	char action;
	std::cout << "Player is in W_Drive Space\n";
	player->set_action_pallet("eid");
	has_items();
	action = player->get_action();
	
	switch(action)
	{
		case 'e' :	move_player(0);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
		case 'd' :	this->space_inventory->run();
					break;
	}
}
