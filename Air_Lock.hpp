/*********************************************************************
** Filename: Air_Lock.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef AIR_LOCK_HPP
#define AIR_LOCK_HPP

#include "Space.hpp"

class Air_Lock : public Space
{
	private:
	
		bool deleted_outside;
		static COORD AIR_LOCK_MESSAGE;
		File_Art *art;
		bool added_outside;
	
	public:
		
		Air_Lock(Player *player);
		virtual ~Air_Lock();
		virtual void special();
		virtual void link();
		void add_outside_spaces();
		void space_walk_validity();
		void check_objectives();
};
#endif
