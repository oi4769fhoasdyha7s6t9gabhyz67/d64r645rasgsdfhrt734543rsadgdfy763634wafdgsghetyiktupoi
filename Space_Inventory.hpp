/*********************************************************************
** Filename: Inventory.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef SPACE_INVENTORY_HPP
#define SPACE_INVENTORY_HPP

#include "Inventory.hpp"
#include "Player.hpp"

class Space_Inventory : public Inventory
{
	public:
		void add_item(int);
		void remove_item(int);
		Space_Inventory(Player *player);
		virtual ~Space_Inventory() {}
};

#endif
