/*********************************************************************
** Filename: Bob.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Bob.hpp"
#include "Player.hpp"

Bob::Bob(Player *player) : Space("bob_space", bob_space, player)
{
	bad = new File_Art("Art/command_bob.ans");
	good = new File_Art("Art/command_bob_good.ans");
	art = bad;
	can_drop = false;
}
Bob::~Bob()
{
	art = NULL;
	DELETE(bad);
	DELETE(good);
}

void Bob::link()
{
	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[command];
}

void Bob::special()
{	
	erase->draw();
	art->draw();
	char action;
	std::cout << "Player is in Bob Space\n";
	player->set_action_pallet("cpi");
	action = player->get_action();
	
	switch(action)
	{
		case 'c' :	move_player(0);
					break;
		case 'p' :	carry_bob();
					break;
		case 'i' :	player->get_inventory()->run();
					break;
	}
}

void Bob::carry_bob()
{
	if (player->get_inventory()->add_item(bob) == true)
	{
		art = good;
	}
}


