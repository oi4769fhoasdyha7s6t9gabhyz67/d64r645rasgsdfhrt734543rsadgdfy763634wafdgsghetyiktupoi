/*********************************************************************
** Program Filename:  File_Art.cpp
** Author: Kruno Peric
** Date: 6/29/2016
** Description: 
** Input:  
** Output: 
*********************************************************************/

#include "File_Art.hpp"
#include <iostream>

File_Art::File_Art(std::string file_name, std::string color, short int x, short int y)
: Art(x, y)
{
	this->file_name = file_name;	
	this->color = color;
	file.open(file_name.c_str(), std::ios::in);
}
void File_Art::erase()
{
}
void File_Art::displace()
{
}
void File_Art::draw()
{
	place_cursor(position);
	std::cout << color << std::flush;
	std::string file_line_input;
	file.seekg(0L, std::ios::beg);
	char ch = file.peek();
	while(file.fail() == false && ch != EOF)
	{
		file.get(ch);
		std::cout << ch;
		//std::getline(file, file_line_input);
		//std::cout << file_line_input << std::endl;
		//std::cout << std::flush << "\033[" << position.x << "G" << std::flush;
		if (ch == '\n')
			std::cout << std::flush << "\033[" << position.x << "G" << std::flush;

		ch = file.peek();
	}
	std::cout << std::flush  << "\033[0m" << std::flush;
}
