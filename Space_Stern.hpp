/*********************************************************************
** Filename: Space_Stern.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef SPACE_STERN_HPP
#define SPACE_STERN_HPP

#include "Space.hpp"

class Space_Stern : public Space
{
	private:
		
		File_Art *art;
		
	public:
		
		Space_Stern(Player *player);
		virtual ~Space_Stern();
		void link();
		virtual void special();
		
};
#endif
