/***********************************************************************
** Filename: 
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>
#include "Inventory.hpp"

class Space;	// Forward Declarations since they have each other
class Inventory;

class Player
{
	private:

		/*
		 * action_pallete is the current available options for a player to select and
		 * execute.  It is a string that changes depending on where the player is in
		 * the game.
		 */

		std::string action_pallete;

		 /* space the Space object that the player currently resides in. */

		Space *space;

		/* the player's item inventory */

		Inventory * inventory;


		bool won;

		int timer;

		int limit;

	public:

		Player();
		~Player();
		
		/*
		 * get_action() prompt's the user to select and enter an action from the
		 * currently avabilable action_pallete.  The character the user enters is
		 * returned.
		 *
		 */

		char get_action();

		/*
		 * returns true if the the string is one character long, and either a lower
		 * case letter or number.
		 */

		bool input_valid(std::string);

		void set_action_pallet(std::string action_pallete);
		void set_space(Space *space);
		
		std::string get_action_pallette();
		Space * get_space();
		Inventory * get_inventory();

		bool get_won();
		void set_won(bool);	
		int get_timer();
		int get_limit();
};
#endif 
