/*********************************************************************
** Filename: Engineering.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Engineering.hpp"
#include "Player.hpp"

Engineering::Engineering(Player *player) : Space("engineering", engineering, player)
{
	art = new File_Art("Art/engineering.ans");
}

Engineering::~Engineering()
{
	DELETE(art);
}

void Engineering::link()
{
	std::cout << "running link() in Engineering...\n";

	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[hallway];
	accessible_spaces[1] = space_list[maintenance];
	accessible_spaces[2] = space_list[coolant_skid];
	accessible_spaces[3] = space_list[coolant_console];
	accessible_spaces[4] = space_list[w_drive];
}

void Engineering::special()
{
	erase->draw();
	art->draw();
	char action;
	player->set_action_pallet("hmscwid");
	has_items();
	action = player->get_action();
	
	switch(action)
	{
		case 'h' :	move_player(0);
					break;
		case 'm' :	move_player(1);
					break;
		case 's' :	move_player(2);
					break;
		case 'c' :	move_player(3);
					break;
		case 'w' :	move_player(4);
					break;
		case 'i' :	player->get_inventory()->run();
					break;
		case 'd' :	this->space_inventory->run();
					break;
	}
}
