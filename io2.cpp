/*********************************************************************
** File Name: io2.cpp
** Author: Kruno Peric
** Date: 7/3/2016
** Description: Implimentation for iostream wrapper function library.
** Parameters: 
** Pre-Conditions: 
** Post-Conditions: ouput stream
*********************************************************************/

#include "io2.hpp"
#include <iostream>
#include <cmath>

/*********************************************************************
** Function: is_int()
** Description: Returns true if the string is an integer, returns false otherwise.
** Parameters: std::string string_in 
** Pre-Conditions: this headerfile and iostream	
** Post-Conditions: 
*********************************************************************/

bool io2::is_int(std::string string_in, bool error_message)
{
	char character;
	#define INVALID	\
		if (error_message == true)\
		{\
			std::cout << "\033[31mInvalid: Please enter an integer\033[0m" <<\
				std::endl;\
		}\
		return false;


	if (string_in == "")
	{
		INVALID;
	}

	for (unsigned int i=0; i<string_in.length(); i++)
	{
		character = string_in[i];

		/* 
		 * Special test case for first digit because it is allowed to have a
		 * hyphen...
		 */

		if (i == 0)
		{
			if ( !(character >= 48 && character <= 57) && character != 45 )
			{
				INVALID;
			}
		}
		else
		{
			/* check the ascii value of the ith character in the string */

			if ( !(character >= 48 && character <= 57) )
			{
				INVALID;
			}
		}
	}
	return true;
}




/*********************************************************************
** Function: string_to_double()
** Description: Converts string to double.
** Parameters: std::string string_in 
** Pre-Conditions: String must only containg numbers
** Post-Conditions: 
*********************************************************************/

double io2::string_to_double(std::string string_in)
{
	int digit;	// the integer value of digit in a particular decimal place
	int decimal_place = string_in.length() - 1;
	double geometric_summation = 0;
	int sign = 1;

	for (unsigned int i=0; i<string_in.length(); i++)
	{
		/*
		 * digit is calculated by offsetting the ascii value of the ith string 
		 * character to generate the correct number value for the digit it represents.
		 * The first character is checked to see if it is a hyphen for negative
		 * vales.
		 */
	
		if (!(i == 0 && string_in[0] == 45))
		{
			digit = string_in[i] - '0';

			geometric_summation += digit * std::pow(10, decimal_place);
			decimal_place--;		
		}
		else
		{
			decimal_place--;		
			sign = -1;
		}
	}
	return sign * geometric_summation;
}




/*********************************************************************
** Function: dbl_valid_range()
** Description: Checks if a double is within range [MIN..MAX]
** Parameters: value and boundaries 
** Pre-Conditions: this headerfile and iostream	
** Post-Conditions: 
*********************************************************************/

bool io2::dbl_valid_range(double value, const int MIN, const int MAX, bool error_message)
{
	if ((value >=  MIN) && (value <= MAX))
	{
		return true;
	}
	else
	{
		if (error_message == true)
		{
			std::cout << "\033[031mInvalid: Please enter a value in range [" << MIN 
			<< ".." << MAX << "]\033[0m" << std::endl;
		}
		return false;
	}
}




/*********************************************************************
** Function: int_range_in()
** Description: Range control and prompt and validation for reading int.
** Parameters: boundaries 
** Pre-Conditions: this headerfile and iostream	
** Post-Conditions: 
*********************************************************************/

int io2::int_range_in(int MIN, int MAX)
{
	std::string streamed_string;
	double value;
	bool invalid_input = true;

	do
	{

		getline(std::cin, streamed_string);
		if (io2::is_int(streamed_string, true) == true)
		{
			value = string_to_double(streamed_string);
			if (io2::dbl_valid_range(value, MIN, MAX) == true)
			{
				invalid_input = false;
			}
		}
	}
	while (invalid_input == true );

	return value;
}





/*********************************************************************
** Function: cin_st()
** Description: Reads string with no spaces.  includes prompt.
** Parameters: prompt 
** Pre-Conditions: this headerfile and iostream	
** Post-Conditions: 
*********************************************************************/

std::string io2::cin_string_no_space(std::string prompt)
{
	std::string s;
	
	if (prompt != "")
	{
		std::cout << prompt ;
	}

	std::cin >> s;
	std::cin.clear();
	std::cin.ignore(10000, '\n');		// not ideal, will research using getline()
										// or something else...
	return s;
}




/*********************************************************************
** Function: cin_ch()
** Description: Reads.  includes prompt.
** Parameters: prompt 
** Pre-Conditions: this headerfile and iostream	
** Post-Conditions: 
*********************************************************************/

char io2::cin_ch(bool stand_message, std::string prompt)
{
	char c;
	
	if (prompt != "")
	{
		std::cout << prompt ;
	}
	else if (stand_message == true)
	{
		std::cout << "Enter Character: ";
	}

	std::cin >> c;
	std::cin.clear();
	std::cin.ignore(10000, '\n');		// not ideal, will research using getline()
										// or something else...
	return c;
}




/*********************************************************************
** Function: enter()
** Description: just continues when the user enters something..
** Parameters:
** Pre-Conditions: 
** Post-Conditions: 
*********************************************************************/

void io2::enter()
{
	std::string garbage;
	std::cout<<"[Press ENTER to continue....]";
	std::getline(std::cin, garbage);
}
