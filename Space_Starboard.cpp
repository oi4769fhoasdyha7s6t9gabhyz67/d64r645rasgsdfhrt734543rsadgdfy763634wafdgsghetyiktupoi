/*********************************************************************
** Filename: Space_Starboard.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Space_Starboard.hpp"
#include "Player.hpp"

Space_Starboard::Space_Starboard(Player *player) : Space("space_starboard", space_starboard, player)
{
	bad = new File_Art("Art/space_starboard.ans");
	good = new File_Art("Art/space_starboard_fixed.ans");
	art = bad;
	can_drop = false;
}

Space_Starboard::~Space_Starboard()
{
	art = NULL;
	DELETE(bad);
	DELETE(good);
}

void Space_Starboard::link()
{
	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[space_stern];
}

void Space_Starboard::special()
{
	erase->draw();
	art->draw();
	char action;
	std::string action_pallete = "si";
	if (comm_sat_fixed == false)
	{
		action_pallete += 'r';
	}
	player->set_action_pallet(action_pallete);

	action = player->get_action();
	
	switch(action)
	{
		case 's' :	move_player(0);
					break;
		case 'r' :	fix_wires();
					break;
		case 'i' :	player->get_inventory()->run();
					break;
	}
}

void Space_Starboard::fix_wires()
{
	if (player->get_inventory()->has_item(7) == true)
	{
		comm_sat_fixed = true;
		art = good;
	}
	else
	{
		std::cout << "\033[1;31mYou can only fix this using the small tool kit..." <<
			"\033[0m";
		io2::enter();
	}
}
