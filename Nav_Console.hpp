/*********************************************************************
** Filename: Nav_Console.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef NAV_CONSOLE_HPP
#define NAV_CONSOLE_HPP

#include "Space.hpp"

class Nav_Console : public Space
{
	private:
	
		File_Art *bad;
		File_Art *good;
		File_Art *art;
		File_Art *message;
	
	public:
		
		Nav_Console(Player *player);
		virtual ~Nav_Console();
		virtual void special();
		void link();
		void win_check();
};
#endif
