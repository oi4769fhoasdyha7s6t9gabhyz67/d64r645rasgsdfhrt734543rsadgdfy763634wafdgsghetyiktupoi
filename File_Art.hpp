/*********************************************************************
 ** Program Filename:  File_Art.hpp
** Author: Kruno Peric
** Date: 6/29/2016
** Description: 
** Input:  
** Output: 
*********************************************************************/

#ifndef FILE_ART_HPP
#define FILE_ART_HPP

#include "Art.hpp"
#include <fstream>
#include <iostream>

class File_Art : public Art
{
	private:
	
		std::string file_name;
		std::string color;
		std::ifstream file;

	public:
		
		File_Art(std::string file_name, std::string color = "\033[0m", 
			short int x = DEFAULT_POS.x, short int y = DEFAULT_POS.y);
		~File_Art() {};

		virtual void draw();
		virtual void erase();	// perhaps not virtual?
		virtual void displace();
};
#endif
