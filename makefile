CXX = g++
#CXXFLAGS = -std=c++11 
CXXFLAGS += -Wall 
CXXFLAGS += -pedantic-errors
CXXFLAGS += -g
#CXXFLAGS += -O3
#LDFLAGS = -lboost_date_time


OBJS = main.o Space.o File_Art.o Art.o globals.o Player.o io2.o
OBJS += Structure.o Command.o Nav_Console.o Systems_Console.o Comms_Console.o
OBJS += Hallway.o Comms.o Sick_Bay.o Engineering.o Air_Lock.o
OBJS += Maintenance.o Coolant_Skid.o Coolant_Console.o W_Drive.o
OBJS += Inventory.o Space_Inventory.o Bob.o
OBJS += Space_Stern.o Space_Port.o Space_Starboard.o

SRCS = main.cpp Space.cpp File_Art.cpp Art.cpp globals.cpp Player.cpp io2.cpp
SRCS += Structure.cpp Command.cpp Nav_Console.cpp Systems_Console.cpp Comms_Console.cpp
SRCS += Hallway.cpp Comms.cpp Sick_Bay.cpp Engineering.cpp Air_Lock.cpp
SRCS += Maintenance.cpp Coolant_Skid.cpp Coolant_Console.cpp W_Drive.cpp
SRCS += Inventory.cpp Space_Inventory.cpp Bob.cpp
SRCS += Space_Stern.cpp Space_Port.cpp Space_Starboard.cpp

HEADERS = Space.hpp File_Art.hpp Art.hpp globals.hpp Player.hpp io2.hpp
HEADERS += Structure.hpp Command.hpp Nav_Console.hpp Systems_Console.hpp Comms_Console.hpp
HEADERS += Hallway.hpp Comms.hpp Sick_Bay.hpp Engineering.hpp Air_Lock.hpp
HEADERS += Maintenance.hpp Coolant_Skid.hpp Coolant_Console.hpp W_Drive.hpp
HEADERS += Inventory.hpp Space_Inventory.hpp Bob.hpp
HEADERS += Space_Stern.hpp Space_Port.hpp Space_Starboard.hpp


#target: dependencies
#	rule to build
#
PROG = main

${PROG}: ${SRCS} ${HEADERS}
	${CXX} ${CXXFLAGS} ${SRCS} -o ${PROG}

clean:
	rm -f ${PROG} *.o *~ 
