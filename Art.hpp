/*********************************************************************
** Program Filename:  Art.hpp
** Author: Kruno Peric
** Date: 6/29/2016
** Description: 
** Input:  
** Output: 
*********************************************************************/

#ifndef ART_HPP
#define ART_HPP

#include "globals.hpp"

class Art
{
	protected:

		COORD position;			// placement of art.handle in the terminal screen
		COORD handle;			// (0,0) COORD offset for art data
		COORD overall_size;		// x = width, y = length
		COORD vector;			// (i,j) displacement vectors
	
	public:
		
		Art(short int x = DEFAULT_POS.x, short int y = DEFAULT_POS.y);
		virtual void draw() = 0;
		virtual void erase() = 0;	// perhaps not virtual?
		virtual void displace() = 0;
		void set_position();
		virtual ~Art() {};
};
#endif
