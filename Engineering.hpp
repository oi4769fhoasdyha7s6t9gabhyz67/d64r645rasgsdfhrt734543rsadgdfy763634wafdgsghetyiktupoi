/*********************************************************************
** Filename: Engineering.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef ENGINEERING_HPP
#define ENGINEERING_HPP

#include "Space.hpp"

class Engineering : public Space
{
	private:

		File_Art *art;

	public:
		
		Engineering(Player *player);
		virtual ~Engineering();
		virtual void special();
		virtual void link();

};
#endif
