/*********************************************************************
** Filename: Space_Starboard.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef SPACE_STARBOARD_HPP
#define SPACE_STARBOARD_HPP

#include "Space.hpp"

class Space_Starboard : public Space
{
	private:
		
		File_Art *bad;
		File_Art *good;
		File_Art *art;
	
	public:
		
		Space_Starboard(Player *player);
		virtual ~Space_Starboard();
		virtual void special();
		void link();
		void fix_wires();
};
#endif
