/*********************************************************************
** Filename: main.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include <iostream>
#include "Art.hpp"
#include "File_Art.hpp"
#include <cstdlib>
#include "Space.hpp"
#include "Player.hpp"
#include <unistd.h>
#include "io2.hpp"
#include "Structure.hpp"
#include "Command.hpp"

int main()
{
	std::cout << "\033[1;1H" << "\033[2J";

	std::cout << "\033[1;33mThis game requires a console with at least 70 characters \n"
	<<                     "by 190 characters wide.  It uses ANSI escape graphics. \n"
	<<                     "It was test in gnome terminal, terminator and windows cmd \n"
	<<                     "successfully using a monospace 10pt font with a \n"
	<<                     "238w x 70t sized console window.  Please use default \n"
	<<					   "text colors of white text on black background.\n\n\n\n\n";

	std::cout << "\033[0m" <<

				"You're the ship engineer for a private intergalactic space vessel.  \n" <<
				"There are only two people aboard your large, and sophisticated \n" <<
				"space-craft.  You, and the captain.\n\n"<<

				"During a routine transport job, your vessel was attacked by \n" << 
				"smuggler ships.  You managed to escape, but not without giving \n" <<
				"and taking everything the ship's got...\n\n" <<

				"In a last ditch effort, you ran the warp drive to full capacity\n" <<
				" and managed to escape without a trace.  However, your ship is \n" <<
				"dead in the water due to the attacks as well as system stress.\n" <<

				"Your captain is also severly hurt.  Your mission is to repair the \n" <<
				"ship to working order so that you could fly it a medical facility \n" <<
				"in order to save your captains life.  You are light years away \n " <<
				"from any intelligant life, stranded in deep space.  You have no \n" <<
				"choice but to get that ship running and save your captain!\n\n"

				"important:  your captain's name is \"bob\", and you should \n" <<
				"probably find him and check on him first...\n" << std::endl;

	io2::enter();

	std::cout << "\033[1;1H" << "\033[2J";

	Player player;
	Structure space_ship(&player);

	do
	{
		player.get_space()->special();
		if (player.get_won() == true)
		{
			break;
		}
	}
	while (player.get_timer() < player.get_limit());

	if ( player.get_won() == false)
	{
		place_cursor(DEFAULT_POS);
		std::cout << "\033[2J";
		std::cout << "\033[0myou loose\n\n\n\n\n" << std::endl;
	}

	return 0;
}
