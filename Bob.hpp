/*********************************************************************
** Filename: Bob.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef BOB_HPP
#define BOB_HPP

#include "Space.hpp"

class Bob : public Space
{
	private:

		File_Art *art;
		File_Art *good;
		File_Art *bad;

	public:
		
		Bob(Player *player);
		virtual ~Bob();
		virtual void special();
		void link();
		void carry_bob();
};
#endif
