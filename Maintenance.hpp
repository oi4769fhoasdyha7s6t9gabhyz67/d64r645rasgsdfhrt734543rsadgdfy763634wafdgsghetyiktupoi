/*********************************************************************
** Filename: Maintenance.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef MAINTENANCE_HPP
#define MAINTENANCE_HPP

#include "Space.hpp"

class Maintenance : public Space
{
	private:

		File_Art *art;
		
	public:
		
		Maintenance(Player *player);
		virtual ~Maintenance();
		virtual void special();
		virtual void link();

};
#endif
