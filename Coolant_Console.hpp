/*********************************************************************
** Filename: Coolant_Console.hpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: Interface for a Space class node
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#ifndef COOLANT_CONSOLE_HPP
#define COOLANT_CONSOLE_HPP

#include "Space.hpp"

class Coolant_Console : public Space
{
	private:
	
		File_Art *art;
		File_Art *good;
		File_Art *valve;
		File_Art *bad;
		File_Art *pid_leak;
		File_Art *pid_valve;
		File_Art *pid_good;
		File_Art *pid;
		File_Art *leak_message;
		File_Art *valve_message;
		File_Art *terminal_main_eraser;

	public:
		
		Coolant_Console(Player *player);
		virtual ~Coolant_Console();
		virtual void special();
		virtual void link();
		void process_diagram();

};
#endif
