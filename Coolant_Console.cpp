/*********************************************************************
** Filename: Coolant_Console.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Coolant_Console.hpp"
#include "Player.hpp"

Coolant_Console::Coolant_Console(Player *player) : Space("coolant_console", coolant_console, player)
{
	bad = new File_Art("Art/coolant_console_bad.ans");
	valve = new File_Art("Art/coolant_console_valve.ans");
	good = new File_Art("Art/coolant_console_good.ans");
	art = bad;
	pid_leak = new File_Art("Art/pid_leak.ans", "\033[0m", TERMINAL_MAIN.x, TERMINAL_MAIN.y);
	pid_valve = new File_Art("Art/pid_valve.ans", "\033[0m", TERMINAL_MAIN.x, TERMINAL_MAIN.y);
	pid_good = new File_Art("Art/pid_good.ans", "\033[0m", TERMINAL_MAIN.x, TERMINAL_MAIN.y);
	pid = pid_leak;

	leak_message = new File_Art("Art/leak_message.txt", "\033[0m", TERMINAL_MESSAGE.x,
		TERMINAL_MESSAGE.y);
	
	valve_message = new File_Art("Art/valve_message.txt", "\033[0m", TERMINAL_MESSAGE.x,
		TERMINAL_MESSAGE.y);

	terminal_main_eraser = new File_Art("Art/terminal_main_eraser.txt", "\033[0m",
		TERMINAL_MAIN.x, TERMINAL_MAIN.y);

	can_drop = false;
}

Coolant_Console::~Coolant_Console()
{
	art = NULL;
	DELETE(bad);
	DELETE(valve);
	DELETE(good);
	pid = NULL;
	DELETE(pid_leak);
	DELETE(pid_valve);
	DELETE(pid_good);
	DELETE(leak_message);
	DELETE(valve_message);
	DELETE(terminal_main_eraser);
}

void Coolant_Console::link()
{
	std::cout << "running link() in Coolant_Console...\n";

	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[engineering];
}

void Coolant_Console::special()
{
	erase->draw();
	art->draw();
	char action;
	player->set_action_pallet("ep");
	action = player->get_action();
	
	switch(action)
	{
		case 'e' :	move_player(0);
					break;
		case 'p' :	process_diagram();
					accessed_coolant_console = true;
					break;
	}
}

void Coolant_Console::process_diagram()
{
	if (leak == true)
	{
		terminal_main_eraser->draw();
		pid_leak->draw();
		leak_message->draw();
		io2::enter();
		if (panel_fixed == true)
		{
			art = valve;
			leak = false;
		}
	}
	else if (bad_valve == true)
	{
		terminal_main_eraser->draw();
		pid_valve->draw();
		valve_message->draw();
		io2::enter();
		if (valve_replaced == true)
		{
			coolant_online = true;
			art = good;
			bad_valve = false;
		}
	}
	else
	{
		terminal_main_eraser->draw();
		pid_good->draw();
		place_cursor(TERMINAL_MESSAGE);
		io2::enter();
	}
} 
