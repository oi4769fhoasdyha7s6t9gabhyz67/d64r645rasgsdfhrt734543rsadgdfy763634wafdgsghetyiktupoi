/*********************************************************************
** Filename: Comms_Console.cpp
** Author: Kruno Peric
** Date: 8/2/2016
** Description: 
** Parameters: 
** Input:
** Output:
** *********************************************************************/

#include "Comms_Console.hpp"
#include "Player.hpp"

Comms_Console::Comms_Console(Player *player) : Space("comms_console", comms_console, player)
{
	bad = new File_Art("Art/comms_bad.ans");
	good = new File_Art("Art/comms_good.ans");
	art = bad;
	can_drop = false;
}

Comms_Console::~Comms_Console()
{
	DELETE(bad);
	DELETE(good);
	art = NULL;
}

void Comms_Console::link()
{
	/* 
	 * All of he accessible spaces from this space will be linked.  This is only to
	 * be done AFTER all the objects have been instantiated in to the master object
	 * list in the Sturucture class...
	 */

	accessible_spaces[0] = space_list[command];
}

void Comms_Console::special()
{
	erase->draw();
	art->draw();
	char action;
	player->set_action_pallet("cl");

	action = player->get_action();
	
	switch(action)
	{
		case 'c' :	move_player(0);
					break;
		case 'l' :	uplink();
					accessed_comms_console = true;
					break;
	}
}

void Comms_Console::uplink()
{
	if (comm_sat_fixed == true)
	{
		comms_online = true;
		art = good;
	}
	else
	{
		COORD COMMS_TERM = {77,13};
		place_cursor(COMMS_TERM);
		std::cout << "\033[33m>>> Uplink Unsuccessful...Press Enter to Continue...\033[0m";
		io2::enter();
	}
}
